(async () => {
    require('fs').rmdirSync('./dist', { recursive: true });
    await new (require('parcel-bundler'))('index.html', { publicURL: '.' }).bundle();
    process.exit();
})();